package com.example.miro.test.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.miro.test.dao.SimpleIdGenerator;
import com.example.miro.test.dao.WidgetStore;
import com.example.miro.test.domain.Widget;
import java.util.List;
import org.junit.jupiter.api.Test;

public class WidgetServiceIT {

  private SimpleIdGenerator idGenerator = new SimpleIdGenerator();
  private WidgetStore widgetStore = new WidgetStore(idGenerator);
  private WidgetService widgetService = new WidgetService(widgetStore);

  @Test
  public void createSimple() {
    // given
    Widget widget = widget(3);

    // when
    Widget created = widgetService.create(widget);
    List<Widget> list = widgetService.list();

    // then
    assertThat(created).isEqualToIgnoringGivenFields(widget, "id");
    assertThat(list).extracting("z").containsOnly(3);
  }

  @Test
  public void createRepeatedShiftsUpper() {
    // given
    Widget widget = widget(3);
    Widget widget2 = widget(3);

    // when
    Widget first = widgetService.create(widget);
    Widget second = widgetService.create(widget2);

    // then
    assertThat(second.getZ()).isEqualTo(3);
    assertThat(widgetService.byId(first.getId()).getZ()).isEqualTo(4);
  }

  @Test
  public void createNoZIndexInsertsAtTop() {
    // given
    Widget widget = widget(3);
    Widget widget2 = widget(3);
    widget2.setZ(null);

    // when
    Widget first = widgetService.create(widget);
    Widget second = widgetService.create(widget2);

    // then
    assertThat(second.getZ()).isEqualTo(4);
    assertThat(widgetService.byId(first.getId()).getZ()).isEqualTo(3);
  }

  @Test
  public void createWithoutZIndex() {

  }



  private Widget widget(int z) {
    Widget widget = new Widget();
    widget.setZ(z);
    widget.setX(1);
    widget.setY(2);
    widget.setHeight(4);
    widget.setWidth(5);
    return widget;
  }


}
