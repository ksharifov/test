package com.example.miro.test.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.miro.test.common.exception.WidgetNotFoundException;
import com.example.miro.test.common.exception.WidgetUpdateException;
import com.example.miro.test.dao.WidgetStore;
import com.example.miro.test.domain.Widget;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WidgetServiceTest {

  @Mock
  private WidgetStore widgetStore;

  @InjectMocks
  private WidgetService widgetService;


  @Test
  void create_whenNoZShouldInsertAtTop() {
    // given
    Widget widget = widget();
    widget.setZ(null);

    // when
    widgetService.create(widget);

    // then
    verify(widgetStore).insertAtTop(widget);
  }

  @Test
  void create_whenExistingZShouldShiftOthers() {
    // given
    Widget widget = widget();
    widget.setZ(3);
    when(widgetStore.containsIndex(3))
      .thenReturn(true);

    // when
    widgetService.create(widget);

    // then
    verify(widgetStore).insertAndShift(widget);
  }

  @Test
  void create_whenNonExistingZShouldInsert() {
    // given
    Widget widget = widget();
    widget.setZ(3);
    when(widgetStore.containsIndex(3))
      .thenReturn(false);

    // when
    widgetService.create(widget);

    // then
    verify(widgetStore).insertNew(widget);
  }

  @Test
  void update() {
    // given
    Widget widget = widget();
    Widget widgetToReturn = widget();
    when(widgetStore.find(widget.getId()))
      .thenReturn(widgetToReturn);

    // when - then
    widgetService.update(widget.getId(), widget);

    // then
    verify(widgetStore).update(widget.getId(), widget);
  }

  @Test
  void updateShouldThrowExceptionWhenInvalid() {
    // given
    Widget widget = widget();
    widget.setX(null);

    // same widget values, but with X set
    Widget widgetToReturn = widget();
    when(widgetStore.find(widget.getId()))
      .thenReturn(widgetToReturn);

    // when - then
    assertThatThrownBy(() -> widgetService.update(widget.getId(), widget))
      .isInstanceOf(WidgetUpdateException.class);
  }

  @Test
  void update_shouldThrowExceptionWhenNotFound() {
    // given
    Widget widget = widget();

    // widget not found in storage
    when(widgetStore.find(widget.getId()))
      .thenReturn(null);

    // when - then
    assertThatThrownBy(() -> widgetService.update(widget.getId(), widget))
      .isInstanceOf(WidgetNotFoundException.class);
  }

  @Test
  void delete() {
    // given
    Widget widget = widget();
    Widget widgetToReturn = widget();
    when(widgetStore.find(widget.getId()))
      .thenReturn(widgetToReturn);

    // when
    widgetService.delete(widget.getId());

    // when
    verify(widgetStore).remove(widget.getId());

  }

  @Test
  void delete_shouldThrowExceptionWhenNotFound() {
    // given
    Widget widget = widget();

    // widget not found in storage
    when(widgetStore.find(widget.getId()))
      .thenReturn(null);

    // when - then
    assertThatThrownBy(() -> widgetService.delete(widget.getId()))
      .isInstanceOf(WidgetNotFoundException.class);

  }

  private Widget widget() {
    Widget widget = new Widget();
    widget.setId(1000);
    widget.setX(1);
    widget.setY(2);
    widget.setZ(3);
    widget.setHeight(4);
    widget.setWidth(5);
    return widget;
  }
}