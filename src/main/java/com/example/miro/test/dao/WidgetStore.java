package com.example.miro.test.dao;

import com.example.miro.test.domain.Widget;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.assertj.core.util.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WidgetStore {

  /**
   * id <-> widget map, quick access to widgets by id
   */
  private Map<Long, Widget> widgets = new HashMap<>();

  /**
   * z-index <-> widget sorted map, maintains order of the z-indices
   */
  private TreeMap<Integer, Widget> zToId = new TreeMap<>();


  @Autowired
  private final SimpleIdGenerator idGenerator;

  public WidgetStore(SimpleIdGenerator idGenerator) {
    this.idGenerator = idGenerator;
  }


  public void insertNew(Widget widget) {
    long id = idGenerator.next();
    widget.setId(id);
    widget.setLastModified(LocalDateTime.now());

    insert(widget);
  }

  public @Nullable Widget find(@Nonnull Long id) {
    return widgets.getOrDefault(id, null);
  }

  /**
   * Adds widget to the top of the widgets. 'Top' means highest z-index.
   */
  public @Nonnull Widget insertAtTop(Widget widget) {
    Integer maxKey = zToId.lastKey();
    int z = maxKey + 1;
    widget.setZ(z);
    insertNew(widget);
    return widget;
  }

  /**
   * Inserts the widget at widget's Z-index. All upper widgets (with higher z-index) are 'shifted' - i.e. their z-index is incremented by 1.
   * <p/>
   *
   * This method only operates on the {@link WidgetStore#zToId} z-widget map - we update widget's z index inplace, thus no need for accessing {@link
   * WidgetStore#widgets} map
   */
  public void insertAndShift(Widget widget) {
    Integer startZ = widget.getZ();
    shiftFrom(startZ);
    insertNew(widget);
  }

  /**
   * Remove widget from widget map and z-index
   */
  public @Nullable Widget remove(@Nonnull Long id) {
    Widget widget = widgets.remove(id);
    zToId.remove(widget.getZ());
    return widget;
  }

  /**
   * @return all widgets, sorted by z-index
   */
  public @Nonnull List<Widget> loadAll() {
    if (widgets.isEmpty()) {
      return Collections.emptyList();
    }
    return new ArrayList<>(zToId.values());
  }

  public boolean containsIndex(@Nonnull Integer z) {
    return zToId.containsKey(z);
  }

  public void update(@Nonnull Long id, @Nonnull Widget widget) {
    Widget toUpdate = widgets.get(id);
    toUpdate.setX(widget.getX());
    toUpdate.setY(widget.getY());
    toUpdate.setHeight(widget.getHeight());
    toUpdate.setWidth(widget.getWidth());
    toUpdate.setLastModified(LocalDateTime.now());

    moveZIndexIfChanged(id, toUpdate, widget.getZ());
  }

  private void moveZIndexIfChanged(@Nonnull Long id, Widget toUpdate, @Nonnull Integer newZ) {
    if (!toUpdate.getZ().equals(newZ)) {
      // remove to avoid being in the shifted ones
      remove(id);
      toUpdate.setZ(newZ);
      shiftFrom(newZ);
      insert(toUpdate);
    }
  }

  @VisibleForTesting
  void shiftFrom(@Nonnull Integer start) {
    Integer end = zToId.lastKey();
    // shift in reverse order to avoid overriding
    List<Integer> shiftIds = zToId.descendingMap().subMap(end, start - 1)
      .keySet()
      .stream()
      .collect(Collectors.toList());

    for (Integer subZ : shiftIds) {
      Widget wgt = zToId.remove(subZ);
      int newZ = subZ + 1;
      wgt.setZ(newZ);
      zToId.put(newZ, wgt);
    }
  }

  private void insert(@Nonnull Widget widget) {
    widgets.put(widget.getId(), widget);
    zToId.put(widget.getZ(), widget);
  }
}
