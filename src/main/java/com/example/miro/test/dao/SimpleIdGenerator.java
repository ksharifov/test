package com.example.miro.test.dao;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Component;

@Component
public class SimpleIdGenerator {

  private static AtomicLong ids = new AtomicLong();

  public long next() {
    return ids.incrementAndGet();
  }

}
