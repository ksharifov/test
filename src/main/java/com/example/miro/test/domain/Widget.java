package com.example.miro.test.domain;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class Widget {
  private long id;

  private Integer x;
  private Integer y;
  private Integer z;

  private Integer width;
  private Integer height;

  private LocalDateTime lastModified;
}
