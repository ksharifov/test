package com.example.miro.test.api;

import com.example.miro.test.common.exception.WidgetNotFoundException;
import com.example.miro.test.common.exception.WidgetUpdateException;
import com.example.miro.test.domain.Widget;
import com.example.miro.test.service.WidgetService;
import java.util.List;
import javax.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/widgets")
public class WidgetController {

  @Autowired
  private WidgetService widgetService;


  @PostMapping
  public Widget create(@RequestBody Widget widget) {
    return widgetService.create(widget);
  }

  @RequestMapping("/{id}")
  public Widget byId(@PathVariable("id") @Nonnull Long id) {
    Widget widget = widgetService.byId(id);
    if (widget == null) {
      throw new WidgetNotFoundException("Widget not found, id=" + id);
    }
    return widget;
  }

  @PutMapping("/{id}")
  public Widget update(@PathVariable("id") @Nonnull Long id, @RequestBody Widget widget) {
    return widgetService.update(id, widget);
  }

  @DeleteMapping("/{id}")
  public Widget delete(@PathVariable("id") @Nonnull Long id) {
    return widgetService.delete(id);
  }

  @RequestMapping()
  public List<Widget> list() {
    return widgetService.list();
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler( {WidgetNotFoundException.class})
  public void notFound() {
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler( {WidgetUpdateException.class})
  public void invalidParams() {
  }

}
