package com.example.miro.test.common.exception;

public class WidgetUpdateException extends RuntimeException {

  public WidgetUpdateException(String message) {
    super(message);
  }
}
