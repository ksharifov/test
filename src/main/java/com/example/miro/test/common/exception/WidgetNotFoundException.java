package com.example.miro.test.common.exception;

public class WidgetNotFoundException extends RuntimeException {

  public WidgetNotFoundException(String message) {
    super(message);
  }
}
