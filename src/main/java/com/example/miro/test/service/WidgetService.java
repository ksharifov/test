package com.example.miro.test.service;

import com.example.miro.test.common.exception.WidgetNotFoundException;
import com.example.miro.test.common.exception.WidgetUpdateException;
import com.example.miro.test.dao.WidgetStore;
import com.example.miro.test.domain.Widget;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WidgetService {

  private final ReentrantLock lock = new ReentrantLock(true);

  private final WidgetStore widgetStore;

  public WidgetService(@Autowired WidgetStore widgetStore) {
    this.widgetStore = widgetStore;
  }


  public @Nonnull Widget create(Widget widget) {
    Integer zIndex = widget.getZ();
    if (zIndex == null) {
      insertAtTop(widget);
    } else if (existsIndex(zIndex)) {
      insertAndShift(widget);
    } else {
      insert(widget);
    }
    return byId(widget.getId());
  }

  public @Nullable Widget byId(@Nonnull Long id) {
    return widgetStore.find(id);
  }

  public @Nullable Widget delete(@Nonnull Long id) {
    Widget toDelete = byId(id);
    if (toDelete == null) {
      throw new WidgetNotFoundException("Widget not found, provided id=" + id);
    }
    return widgetStore.remove(id);
  }

  public @Nonnull Widget update(@Nonnull Long id, @Nonnull Widget widget) {
    Widget toUpdate = byId(id);
    if (toUpdate == null) {
      throw new WidgetNotFoundException("Widget not found, provided id=" + id);
    }

    validateForUpdate(widget, toUpdate);

    executeLocking(widget, wgt -> widgetStore.update(id, widget));

    return byId(id);
  }

  public @Nonnull List<Widget> list() {
    return widgetStore.loadAll();
  }

  private void insert(@Nonnull Widget widget) {
    executeLocking(widget, widgetStore::insertNew);
  }

  private void insertAndShift(@Nonnull Widget widget) {
    executeLocking(widget, widgetStore::insertAndShift);
  }

  private void insertAtTop(@Nonnull Widget widget) {
    executeLocking(widget, widgetStore::insertAtTop);
  }

  private void executeLocking(@Nonnull Widget widget, Consumer<Widget> consumer) {
    lock.lock();
    try {
      consumer.accept(widget);
    } finally {
      lock.unlock();
    }
  }

  private boolean existsIndex(@Nonnull Integer z) {
    return widgetStore.containsIndex(z);
  }


  private void validateForUpdate(@Nonnull Widget widget, Widget toUpdate) {
    validateAttributeNotRemoved(widget, toUpdate.getX(), widget.getX(), "X");
    validateAttributeNotRemoved(widget, toUpdate.getY(), widget.getY(), "Y");
    validateAttributeNotRemoved(widget, toUpdate.getZ(), widget.getZ(), "Z");
  }

  private void validateAttributeNotRemoved(@Nonnull Widget widget, @Nullable Integer a, @Nullable Integer a2, @Nonnull String propName) {
    if (a != null && a2 == null) {
      throw new WidgetUpdateException("Cannot delete widget property: " + propName + ", id=" + widget.getId());
    }
  }
}
