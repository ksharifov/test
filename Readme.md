#### Running the app
##### From command line
From project root folder

1. `mvn clean spring-boot:run`  
2. `./mvnw spring-boot:run`

##### Test app successfully started:
- `curl http://localhost:8080/miro/ping`, expect 'pong' in response

##### Available endpoints:
###### Create a widget - Http POST on `/miro/widgets`:

```
curl -H "Content-Type: application/json" \
-d '{"x":1, "y": 2, "z":3, "width": 320, "height": 240}' \
-X POST http://localhost:8080/miro/widgets 
```

###### Get a widget by **id** -  Http GET on `/miro/widgets/{id}`:
 
`curl http://localhost:8080/miro/widgets/{x}`, where `{x}` is any `id`. 
E.g. `curl http://localhost:8080/miro/widgets/1` - get a widget by `id==1`

###### Update a widget - Http PUT on `/miro/widgets/{id}` 
```
curl -H "Content-Type: application/json" \
-d '{"x":1, "y": 2, "z":3, "width": 320, "height": 240}' \
-X PUT http://localhost:8080/miro/widgets/1 
```
**id** of the widget must be provided as the path variable

###### Remove a widget - Http DELETE on `/miro/widgets/{id}`
`curl -X DELETE http://localhost:8080/miro/widgets/2`

###### List of widgets - Http GET on `/miro/widgets`: 
`curl http://localhost:8080/miro/widgets`